import re, urllib.request, ssl

class Crawler:
    def crawl(self, dir, url):
        total = 0
        #try:
        context = ssl._create_unverified_context()
        #resource = urllib.request.urlopen(url, context=context)
        #content = resource.read().decode(resource.headers.get_content_charset())
        content = urllib.request.urlopen(url, context=context).read().decode('utf-8')
        imgUrls = re.findall('img .*?src="(.*?)"', content)
        count = 1

        for img in imgUrls:
            url = img
            try:
                img = urllib.request.urlopen(url, context=context).read()
            except ValueError:
                url = 'https:' + img
                img = urllib.request.urlopen(url, context=context).read()
            except urllib.error.URLError as e:
                print('URLError')
                continue
            print('url: ' + url)
            out = open("./static/files/" + dir + "/{0}.jpg".format(count), "wb")
            out.write(img)
            out.close
            count += 1
        #except UnicodeDecodeError as error:
            # print ('UnicodeDecodeError')
            # pass
