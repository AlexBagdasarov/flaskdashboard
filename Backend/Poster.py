import os, requests, datetime, json

class Poster:
    def post(self, dir):
        url = "http://172.19.12.51:9080/powerai-vision/api/dlapis/"+"4bb3606d-eaa8-45cd-a9d7-81e8a579542f"
        path = './static/files/'+dir+'/'

        payload = {
            'Content-Disposition': 'form-data',
            'name': 'files',
        }
        headers = {
            'content-type': "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
            'Cache-Control': "no-cache",
            'Postman-Token': "79772d47-f3fc-4077-9dba-17fc9d53f031"
            }
        f = open(path+'log.txt', 'w')

        for filename in os.listdir(path):
            try:
                files = {
                    'files': open(path+filename, 'rb')
                }
                response = requests.post(url, files=files)
                data = response.json()
                str = json.dumps(data['classified'])
                f.write(filename + ' - ' + str + '\n')
                print (data['classified'])
            except KeyError:
                pass
        f.close()