import datetime, urllib.request, urllib.parse
from Backend.Crawler import Crawler
from Backend.DirectoryGenerating import DirGen
from Backend.Poster import Poster

dirgen = DirGen()
crawler = Crawler()
poster = Poster()

datedirname = datetime.datetime.now().strftime("%Y-%m-%d")

print ('Enter URL to get images: ')
url = input()
surl = urllib.parse.urlsplit(url)[1]
# Check date dir
dirgen.create(datedirname)
# Check url dir
urldir = dirgen.create(datedirname + '/' + surl)
#Start crawling Baby
crawler.crawl(urldir, url)
#Ok, and now let's post this shit to Vision
poster.post(urldir)

