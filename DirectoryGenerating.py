import os

class DirGen:
    def create(self, dirname):
        path = ('./static/files/'+dirname)
        try:
            os.makedirs(path)
        except OSError:
            pass
        return dirname
