import os, ast, ssl, re, urllib.request
from http.cookiejar import CookieJar

str = '{"negative": "0.999"}'
dict = ast.literal_eval(str)
for key in (dict.keys()):
    if key == 'negative':
        print(key,":",dict.get(key))
    else:
        print('not ok')

context = ssl._create_unverified_context()

url = 'https://g-energy.org/catalog/index.html'
content = urllib.request.urlopen(url, context=context).read().decode('utf-8')
imgUrls = re.findall('img .*?src="(.*?)"', content)
count = 1

sheme_url = urllib.parse.urlsplit(url)[0]
netlock_url = urllib.parse.urlsplit(url) [1]

for img in imgUrls:
    url = img
    sheme_img = urllib.parse.urlsplit(img) [0]
    netlock_img = urllib.parse.urlsplit(img) [1]
    try:
        img = urllib.request.urlopen(url, context=context).read()
    except ValueError:
        if netlock_img == '':
            url = sheme_url + '://' + netlock_url + img
        else:
            url = sheme_url + ':' + img
        img = urllib.request.urlopen(url, context=context).read()
    except urllib.error.URLError as e:
        print('URLError')
        continue
    out = open("./static/files/test/{0}.jpg".format(count), "wb")
    out.write(img)
    out.close

    count += 1