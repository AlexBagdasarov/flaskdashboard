import re, urllib.request, ssl
from yattag import Doc

class Crawler:
    def crawl(self, dir, url):
        #try:
        context = ssl._create_unverified_context()
        #resource = urllib.request.urlopen(url, context=context)
        #content = resource.read().decode(resource.headers.get_content_charset())
        content = urllib.request.urlopen(url, context=context).read().decode('utf-8')
        imgUrls = re.findall('img .*?src="(.*?)"', content)
        count = 1

        sheme_url = urllib.parse.urlsplit(url)[0]
        netlock_url = urllib.parse.urlsplit(url)[1]

        img_grid = ''

        for img in imgUrls:
            url = img
            sheme_img = urllib.parse.urlsplit(img)[0]
            netlock_img = urllib.parse.urlsplit(img)[1]
            try:
                img = urllib.request.urlopen(url, context=context).read()
            except ValueError:
                if netlock_img == '':
                    url = sheme_url + '://' + netlock_url + img
                else:
                    url = sheme_url + ':' + img
                img = urllib.request.urlopen(url, context=context).read()
            except urllib.error.URLError as e:
                print('URLError')
                continue
            # out = open("./static/files/test/{0}.jpg".format(count), "wb")
            out = open("./static/files/" + dir + "/{0}.jpg".format(count), "wb")
            out.write(img)
            out.close

            doc, tag, text = Doc().tagtext()
            with tag('li'):
                with tag('a', klass='rig-cell', href='#'):
                    doc.stag('img', klass='rig-img', src='/static/files/'+ dir + '/{0}.jpg'.format(count))
                    with tag('div', klass='text-block', style='background-color: rgba(0,0,0,0.5)'):
                        with tag('h4'):
                            text('Nature')
                        with tag('p'):
                            text('What a beautiful sunrise')
                    with tag('span', klass='rig-overlay'):
                        text('')
                    with tag('span', klass='rig-text'):
                        text('Lorem Ipsum Dolor')
            img_grid += doc.getvalue()

            count += 1

        return img_grid