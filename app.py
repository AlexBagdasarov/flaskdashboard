from flask import Flask, render_template, request, Markup
import ssl, os, urllib.request, re, datetime, urllib.parse
from Crawler import Crawler
from DirectoryGenerating import DirGen
from Poster import Poster
from Mover import Mover

app = Flask(__name__)

dirgen = DirGen()
crawler = Crawler()
poster = Poster()
mover = Mover()

datedirname = datetime.datetime.now().strftime("%Y-%m-%d")

@app.route('/', methods=['GET','POST'])
def main():
    image_grid = ''
    if request.method == 'POST':
        try:
            pippo = request.form.getlist('url[]')
            for item in pippo:
                # return (form.urlInput.data)
                surl = urllib.parse.urlsplit(item)[1]
                # Check date dir
                dirgen.create(datedirname)
                # Check url dir
                urldir = dirgen.create(datedirname + '/' + surl)
                # if surl == 'g-energy.org':
                #     mover.move(urldir)
                # else:
                image_grid = crawler.crawl(urldir, item)
                #image_grid = poster.post(urldir)
        except ValueError:
            pass
    print(image_grid)
    return render_template('index.html',image_grid=image_grid)

@app.route('/upload')
def upload():
    return render_template('upload.html')

@app.route('/archive')
def archive():
    return render_template('archive.html')

@app.route('/cr')
def cr():
    return render_template('confidence_rank.html')

if __name__ == '__main__':
    app.run()