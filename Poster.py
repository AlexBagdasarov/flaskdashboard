import os, requests, json, ast
from yattag import Doc

class Poster:
    def post(self, dir):
        url = "http://172.19.12.51:9080/powerai-vision/api/dlapis/"+"84b84715-3d6c-46f3-b4f2-8f8a3a1c0fd2"
        path = './static/files/' + dir + '/'

        img_grid = ''
        tb_style = ''
        tb_text = ''

        payload = {
            'Content-Disposition': 'form-data',
            'name': 'files',
        }
        headers = {
            'content-type': "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
            'Cache-Control': "no-cache",
            'Postman-Token': "79772d47-f3fc-4077-9dba-17fc9d53f031"
            }

        f = open(path+'log.txt', 'w')

        for filename in os.listdir(path):
            doc, tag, text = Doc().tagtext()
            try:
                files = {
                    'files': open(path+filename, 'rb')
                }
                response = requests.post(url, files=files)
                #Some magick to convert json data -> string -> dict and then make true style
                data = response.json()
                str = json.dumps(data['classified'])
                dict_data = ast.literal_eval(str)
                for key in dict_data.keys():
                    f.write(filename + ' - ' + key + ': ' + dict_data.get(key) + '\n' )
                    if key == 'negative':
                        tb_text = 'Negative'
                        tb_style = 'background-color: rgba(0,0,0,0.5)'
                    elif key == 'Fraud':
                        tb_text = key
                        tb_style = 'background-color: rgba(249,30,3,0.5)'
                    else:
                        tb_text = key
                        tb_style = 'background-color: rgba(20,144,46,0.5)'
                #Generating Image Grid
                with tag('li'):
                    with tag('a', klass='rig-cell', target='_blank', href='http://172.19.12.51:9080/powerai-vision/index.html#!/datasets'):
                        doc.stag('img', klass='rig-img', src='/static/files/' + dir + '/' + filename)
                        with tag('div', klass='text-block', style=tb_style):
                            with tag('h4'):
                                text(tb_text)
                            with tag('p'):
                                text(dict_data.get(key))
                        with tag('span', klass='rig-overlay'):
                            text('')
                        with tag('span', klass='rig-text'):
                            text('Go to PowerAI Vision')
                img_grid += doc.getvalue()
                continue
            except KeyError:
                pass
        f.close()

        return img_grid